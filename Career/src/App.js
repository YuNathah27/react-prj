import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import logo from "./career/careerpath.png";
import "../node_modules/@fortawesome/fontawesome-svg-core/styles.css";
import React from "react";
import { useState } from "react";
import Modal from "./Components/Modal";
import { BrowserRouter as Router, Link, Routes, Route } from "react-router-dom";
import SeniorDetail from "./SeniorDetail";
import Test from "./Test";

function App() {
  const [modalOpen, setModalOpen] = useState(false);

  return (
    
    <div className="App container">
      <div className="content">
      {modalOpen && <Modal setOpenModal={setModalOpen} />}
    </div>
      <h2 className="text-info text-center mt-5">CAREERS</h2>
      <div className="row mt-5">
        <div className="col-lg-6">
        <img src={logo} className="img-fluid" alt="logo" />
        </div>
        <div className="col-lg-6">
          <h3 className="text-info">Benefits</h3>
          <div className="entry-content text-a text-left">
            <ul>
              <li>
                Provide online course and having teacher for new technology and
                Japanese Language.
              </li>
              <li>
                Can work in Japan Country by considering the experience and
                ability etc..
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="col-md-12 mt-5">
        <h3 className="text-center text-info">Career Opportunities</h3>
      </div>
      <div className="row d-flex justify-content-center text-center mt-5">
        <div className="col-lg-7">
          <div className="row">
            <div className="salaryCard col-md-3 al-self-center">
              Basic <br />
              Salary
            </div>
            <div className="col-md-1 al-self-center ml-3">
              {/* <i className="fa fa-plus" style="font-size: 25px; color: #00a4cc"></i> */}
              +
            </div>
            <div className="col-md-8 text-left">
              <ul>
                <li>Technical skill allowance</li>
                <li>Management skill allowance</li>
                <li>Qualification allowance (JLPT, ITPEC, IELTS..)</li>
                <li>Transportation Fees</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-md-5 careerCard text-left">
          <div className="row">
            <div className="col-md-3 text-info">Working<br />Hour</div>
            <div className="col-md-9">
              8:30AM ～ 5:30PM<br />
              (Lunch break:1hour,<br />
              Regular working hours : 8hours)
            </div>
          </div>
          <div className="row pt-2">
            <div className="col-md-3 text-info">Holiday</div>
            <div className="col-md-9">Saturday, Sunday, Public holidays</div>
          </div>
          <div className="row pt-2">
            <div className="col-md-3 text-info">Welfare</div>
            <div className="col-md-9">
              Employee trip<br />
              Regular medical check-up
            </div>
          </div>
        </div>
      </div>
      <div className="row d-flex justify-content-center text-center pt-5">
        <div className="col-md-12">
          <h3 className="text-center text-info">
            We are looking for human resource that meets the following facts
          </h3>
        </div>
        <div className="col-md-8 text-left">
          <ul>
            <li>
              person who wants to perticipate in the Future Myanmar ICT
              development
            </li>
            <li>
              person who can present his own idea and can communicate and
              interact at ease
            </li>
            <li>person who can work with in team</li>
            <li>
              person who is enthusiastic and tries hard to develop own skills
            </li>
            <li>
              person who has future goals and ables to cope with challenges to
              achieve goals
            </li>
          </ul>
        </div>
      </div>
      <div className="row pt-5">
        <div className="col-md-12">
          <h3 className="text-center text-info">Type of Recruiting</h3>
        </div>
        <div className="card col-lg-12 mt-3">
          <div className="row">
            <div className="col-lg-7">
              <h4 className="text-left text-info">
                <span>&#128142;</span> Senior Developer
              </h4>
            </div>
            <div className="col-lg-5 text-left">
              <div className="smallCard">
                <span>&#128182; &nbsp;</span>Negotiable
                <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time &nbsp;
                &nbsp; &nbsp;
                &nbsp; Myanmar-Yangon
              </div>
            </div>
          </div>
          <div className="mt-3 text-left">
            <ul>
              <li>Project experience over 4 years.</li>
              <li>
                At least 3 years in web programming experience with any
                programming language and MVC framework.
              </li>
              <li>Japanese language proficiency level N3 or higher.</li>
              <li>
                Work in a hands-on fashion, building the team - provide
                motivation and Inspiration.
              </li>
            </ul>
          </div>
          <div className="row text-left">
            <div className="col-md-12">
             
              <button
                className="btn btn-info"
                onClick={() => {
                  setModalOpen(true);
                }}
              >
                View Details
              </button>

{/* <Router>
        <Link to="Detail"> Login </Link>
      <Routes>
        <Route path="login" element={<SeniorDetail />} />
      </Routes>
    </Router> */}

              <button className="btn btn-warning">Apply Job</button>
            </div>
          </div>
        </div>
        <div className="card col-lg-12 mt-3">
          <div className="row">
            <div className="col-lg-7">
              <h4 className="text-left text-info">
                <span>&#128142;</span> Junior Developer
              </h4>
            </div>
            <div className="col-lg-5 text-left">
              <div className="smallCard">
                <span>&#128182; &nbsp;</span>Negotiable
                <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time &nbsp;
                &nbsp; &nbsp;
                &nbsp; Myanmar-Yangon
              </div>
            </div>
          </div>
          <div className="mt-3 text-left">
            <ul>
              <li>Any Graduated or Diploma in IT.</li>
              <li>
                Strong technical skills and knowledge with the latest web
                development technology of Git.
              </li>
              <li>Good communication and interpersonal skills.</li>
            </ul>
          </div>
          <div className="row text-left">
            <div className="col-md-12">
              {/* <button className="btn btn-info">
                View Details
              </button> */}
              <Router>
              <Link to="/seniordetail"> View Detail </Link>
              <Routes>
              <Route path="seniordetail" element={<Test />} />
              </Routes>
              </Router>
              <button className="btn btn-warning">Apply Job</button>
            </div>
          </div>
        </div>
        <div className="card col-lg-12 mt-3">
          <div className="row">
            <div className="col-lg-7">
              <h4 className="text-left text-info">
                <span>&#128142;</span> Internship
              </h4>
            </div>
            <div className="col-lg-5">
              <div className="smallCard">
                <span>&#128182; &nbsp;</span>Negotiable
                <span>&nbsp; &nbsp; &#128339; &nbsp;</span>Full Time &nbsp;
                &nbsp; &nbsp;
                &nbsp; Myanmar-Yangon
              </div>
            </div>
          </div>
          <div className="mt-3 text-left">
            <ul>
              <li>Any Graduated or Diploma in IT.</li>
              <li>
                Strong technical skills and knowledge with the latest web
                development technology of Git.
              </li>
              <li>Good communication and interpersonal skills.</li>
            </ul>
          </div>
          <div className="row text-left">
            <div className="col-md-12">
              <button className="btn btn-info">
                View Details
              </button>
              <button className="btn btn-warning">Apply Job</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
