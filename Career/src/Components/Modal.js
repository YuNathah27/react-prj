import React from "react";
import "./Modal.css";
import logo_it from "./career/it.png";
import logo_graduate from "./career/graduate.png";
import logo_jp from "./career/japanese.png";

function Modal({ setOpenModal }) {
  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
          >
            x
          </button>
        </div>
        {/* <!-- JobDetail-Senior --> */}
        {/* <div class="container mb-5 mt-3" id="seniorJobDetail" style="display: none;"> */}
            <div class="row section-title justify-content-center">
                <h3 class="text-info mt-5">SENIOR DEVELOPER</h3>
            </div>
            <div class="row mt-5">
                <div class="col-md-4 text-center">
                <img src={logo_it} className="img-fluid" alt="logo_it" />
                </div>
                <div class="col-md-4 text-center">
                <img src={logo_graduate} className="img-fluid" alt="logo_graduate" />
                </div>
                <div class="col-md-4 text-center">
                <img src={logo_jp} className="img-fluid" alt="logo_jp" />
                </div>
            </div>
            <div class="card mt-5">
                <h3 class="text-info">Job Descriptions</h3>
                <div class="mt-3 text-left">
                    <ul>
                        <li>Create web application by using JAVA, PHP, C#, NEXT JS, REACT, .NET, NODEJS,...</li>
                        <li>Specification, design, coding, testing, release as well as maintenance of software programs.</li>
                        <li>You may also be closely tasked with GM (PM) and team members.</li>
                    </ul>
                </div>
                <hr />
                <h3 class="text-info">Requirements</h3>
                <div class="mt-3 text-left">
                    <ul>
                        <li>Any Graduated</li>
                        <li>More than 3 years in full stack or frontend/backend software development position</li>
                        <li>More than 2 years experience in Database</li>
                        <li>Ability to write good technical documentation.</li>
                        <li>Japanese language proficiency level above N4 or if not we provide training)</li>
                    </ul>
                </div>
                <hr />
                <h3 class="text-info">Recruitment Process</h3>
                <h3 class="text-info mt-5">Benefits</h3>
                <div class="mt-3 text-left">
                    <ul>
                        <li>Free for Japanese language class</li>
                        <li>Saturday, Sunday Holiday</li>
                    </ul>
                </div>
                <hr />
                <h3 class="text-info mt-5">Career Opportunities</h3>
                <div class="mt-3 text-left">
                    <ul>
                        <li>Many chance to work in Japan</li>
                        <li>Can be leader in near future</li>
                    </ul>
                </div>
                <hr />
                <h3 class="text-info mt-5">Message</h3>
                <div class="row mt-3 text-left">
                    <div class="col-md-2">Employment Type</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Full Time</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Working Days</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Monday~Friday</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Working Hours</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">8:30~17:30 (Including 1 hour lunch break)</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Working Place</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Room.204, No.5, Grand Nawaday Condo, Nawaday Street, Dagon Township.Yangon, Myanmar.</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Salary</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Monthly Salary System (Company provides preferential treatments by considering the experience and ability etc..)</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Allowance</div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Qualification Allowance / Overtime Allowance</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Wage Raise </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Twice times in a year (Jan, July)</div>
                </div>
                <div class="row text-left">
                    <div class="col-md-2">Welfare Program  </div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-9 pl-0">Marriage Allowance/ Funeral Service Allowance/ Medical Checkup </div>
                </div>
            </div>
        </div>
      </div>
    // </div>
  );
}

export default Modal;
