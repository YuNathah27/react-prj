import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Career from "./components/career";
import registerServiceWorker from "./registerServiceWorker";
import App from './App';

const DATA = [
    { id: "todo-0", name: "Eat", completed: true },
    { id: "todo-1", name: "Sleep", completed: false },
    { id: "todo-2", name: "Repeat", completed: false }
  ];

// ReactDOM.render(<App tasks={DATA} />, document.getElementById('root'));
ReactDOM.render(<App />, document.getElementById("root"));
// registerServiceWorker();
